

#include <rtthread.h>
#include <rtdevice.h> 
#include <rtconfig.h>

#ifdef PKG_USING_AWTK_DEMO

#include "base/idle.h"
#include "base/timer.h"
#include "tkc/platform.h"
#include "base/main_loop.h"
#include "base/event_queue.h"
#include "base/font_manager.h"
#include "lcd/lcd_mem_fragment.h"
#include "main_loop/main_loop_simple.h"
#include "tkc/mem.h"
#include "tkc/thread.h"


#include "rt_awtk_port.h"
 

 
extern int gui_app_start(int lcd_w, int lcd_h);
extern ret_t platform_prepare(void);


void awtk_thread(void* args) {
   struct rt_device_graphic_info *rt_dev_grap = rt_awtk_port_get_grap_pointer();
  gui_app_start(rt_dev_grap->width, rt_dev_grap->height);

}


int awtk_ui_thread(void)
{
    rt_thread_t thread = RT_NULL;
    rt_awtk_port_lcd_init();
    rt_awtk_port_indev_init();

    platform_prepare();

    thread = rt_thread_create("awtk_thread", 
                              awtk_thread, 
                              RT_NULL, 
                              4096*5,
                              5, 
                              10);
    
    if (thread == RT_NULL){
        return RT_ERROR;
    }
    rt_thread_startup(thread);

    
    return RT_EOK;
}
INIT_APP_EXPORT(awtk_ui_thread);



#endif














// #include <tkc/color.h>
// void flcx00s_fb_test(void)
// {

//     color_t *pColor = (color_t *)_g_rt_dev_grap_info.framebuffer;
//     int i;
//     for(i=0;i<160*800;i++){
//       pColor[i].rgba.a=0x00;
//       pColor[i].rgba.r=0xff;
//       pColor[i].rgba.g=0x00;
//       pColor[i].rgba.b=0x00;
//     }

//     for(;i<320*800;i++){
//       pColor[i].rgba.a=0x00;
//       pColor[i].rgba.r=0x00;
//       pColor[i].rgba.g=0xff;
//       pColor[i].rgba.b=0x00;
//     }
//     for(;i<480*800;i++){
//       pColor[i].rgba.a=0x00;
//       pColor[i].rgba.r=0x00;
//       pColor[i].rgba.g=0x00;
//       pColor[i].rgba.b=0xff;
//     }



// }


 
//  #define pixel_from_rgb(r, g, b)                                                \
//   ((((r) >> 3) << 11) | (((g) >> 2) << 5) | ((b) >> 3))
// #define pixel_to_rgba(p)                                                       \
//   { (0xff & ((p >> 11) << 3)), (0xff & ((p >> 5) << 2)), (0xff & (p << 3)) }


// void flcx00s_fb_test_565(void)
// {

//     uint16_t *pColor = (uint16_t *)_g_rt_dev_grap_info.framebuffer;
//     int i;

//     for(i=0;i<160*800;i++){
//       pColor[i]=    pixel_from_rgb(0xff,0x00,0x00);
//     }

//     for(;i<320*800;i++){
//         pColor[i]=    pixel_from_rgb(0x00,0xff,0x00);
//     }
//     for(;i<480*800;i++){
//         pColor[i]=    pixel_from_rgb(0x00,0x00,0xff);
//     }



// }
/**
 * File:   main_loop_stm32_raw.c
 * Author: AWTK Develop Team
 * Brief:  main loop for stm32
 *
 * Copyright (c) 2018 - 2018  Guangzhou ZHIYUAN Electronics Co.,Ltd.
 *
 * this program is distributed in the hope that it will be useful,
 * but without any warranty; without even the implied warranty of
 * merchantability or fitness for a particular purpose.  see the
 * license file for more details.
 *
 */

/**
 * history:
 * ================================================================
 * 2018-02-17 li xianjing <xianjimli@hotmail.com> created
 *
 */

 
#include "base/idle.h"
#include "base/timer.h"
#include "tkc/platform.h"
#include "base/main_loop.h"
#include "base/event_queue.h"
#include "base/font_manager.h"
#include "lcd/lcd_mem_fragment.h"
#include "main_loop/main_loop_simple.h"
#include "lcd/lcd_mem_rgb565.h"

#include "rt_awtk_port.h"
 
#include <rtthread.h>

static lcd_t *platform_create_lcd(wh_t w, wh_t h) 
{
    lcd_t*   lcd = NULL;
    struct rt_device_graphic_info *rt_dev_grap = rt_awtk_port_get_grap_pointer();

    uint8_t* online_framebuffer  = rt_dev_grap->framebuffer;              // rtthread的fb，写该缓存会直接显示在屏幕上
    uint8_t* offline_framebuffer = rt_malloc(rt_awtk_port_get_fb_len());  // 自己定义的一块内存区，一般是awtk在绘制gui的时候，先绘制到该上面，然后awtk框架会自动把该缓存拷贝到s_framebuffers[0]里面，实现物理屏幕的刷新

    lcd = lcd_mem_rgb565_create_double_fb(w, h, online_framebuffer, offline_framebuffer);
    //lcd = lcd_mem_bgra8888_create_double_fb(w, h, online_framebuffer, offline_framebuffer);

    return lcd;
}

static ret_t platform_disaptch_input(main_loop_t *l) 
{ 
 
    xy_t x = rt_awtk_port_get_touch_x();
    xy_t y = rt_awtk_port_get_touch_y();
    bool_t is_pressed = rt_awtk_port_is_touch_pressed();
    main_loop_post_pointer_event(l, is_pressed, x, y);

    return 0;
}




#include "main_loop/main_loop_raw.inc"

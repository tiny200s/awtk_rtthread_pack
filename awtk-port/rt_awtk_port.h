#ifndef RT_AWTK_PORT_H
#define RT_AWTK_PORT_H

#include <rtthread.h>

void rt_awtk_port_lcd_init(void);
int rt_awtk_port_indev_init(void);
rt_int32_t rt_awtk_port_get_touch_x(void);
rt_int32_t rt_awtk_port_get_touch_y(void);
rt_bool_t rt_awtk_port_is_touch_pressed(void);
struct rt_device_graphic_info * rt_awtk_port_get_grap_pointer(void);
rt_uint32_t rt_awtk_port_get_fb_len(void);

uint64_t get_time_ms64();
void     sleep_ms(uint32_t ms);

#endif
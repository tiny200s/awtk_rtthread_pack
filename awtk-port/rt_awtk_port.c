
#include <rtdevice.h>
#include <rtconfig.h>
#include <touch.h>
#include "rt_awtk_port.h"
#include "drv_touch.h"
#include "drv_fb.h"
#include <stdint.h>
static rt_device_t                   _g_rt_lcd_dev;
static struct rt_device_graphic_info _g_rt_dev_grap_info;

static rt_bool_t  _g_is_pressed = RT_FALSE;
static rt_int32_t _g_last_x = 0;
static rt_int32_t _g_last_y = 0;



#ifdef    RT_USING_TOUCH


static void _rt_send_input_event(touch_message_t *msg)
{
    _g_last_x = msg->x;
    _g_last_y = msg->y;
    switch (msg->event){
        case TOUCH_EVENT_UP:
        case TOUCH_EVENT_NONE:
            _g_is_pressed = RT_FALSE;
        break;
        case TOUCH_EVENT_DOWN:
        case TOUCH_EVENT_MOVE:
            _g_is_pressed = RT_TRUE;
        break;
        default:break;
    }
}

int rt_awtk_port_indev_init(void)
{

    drv_touch_init(_rt_send_input_event);
}
#endif
 
 void rt_awtk_port_lcd_init(void)
{
    /* LCD Device Init */
    _g_rt_lcd_dev = rt_device_find("lcd");
    RT_ASSERT(_g_rt_lcd_dev != RT_NULL);
    if (rt_device_open(_g_rt_lcd_dev, RT_DEVICE_OFLAG_RDWR) == RT_EOK){
        rt_device_control(_g_rt_lcd_dev, RTGRAPHIC_CTRL_GET_INFO, &_g_rt_dev_grap_info);
        rt_kprintf("open lcd is ok\n");
    }

    RT_ASSERT(_g_rt_dev_grap_info.bits_per_pixel == 8 || _g_rt_dev_grap_info.bits_per_pixel == 16 || _g_rt_dev_grap_info.bits_per_pixel == 24
                    || _g_rt_dev_grap_info.bits_per_pixel == 32);

    rt_kprintf("rtt_lcd_init ok %d\n",_g_rt_dev_grap_info.bits_per_pixel);

}





rt_int32_t rt_awtk_port_get_touch_x(void)
{
    return _g_last_x;

}
rt_int32_t rt_awtk_port_get_touch_y(void)
{
    return _g_last_y;

}
rt_bool_t rt_awtk_port_is_touch_pressed(void)
{
    return _g_is_pressed;

}
struct rt_device_graphic_info * rt_awtk_port_get_grap_pointer(void)
{
    return &_g_rt_dev_grap_info;

}
rt_uint32_t rt_awtk_port_get_fb_len(void)
{
    return LCD_BUFFER_SIZE;

}

 
uint64_t get_time_ms64()
{
  return  rt_tick_get();
}

void sleep_ms(uint32_t ms)
{
   rt_thread_delay(ms);
}







# if  0
typedef struct {
    rt_device_t  p_indev;
    rt_sem_t     p_sem;

}rt_awtk_port_indev_t;


static rt_err_t rx_callback(rt_device_t dev, rt_size_t size)
{
    //rt_sem_release(gt9147_sem);
    rt_device_control(dev, RT_TOUCH_CTRL_DISABLE_INT, RT_NULL);
    return 0;
}

static void gt911_interrupt(void * data)
{
     rt_awtk_port_indev_t *p_rt_awtk_indev = (rt_awtk_port_indev_t *)data;
    rt_sem_release(p_rt_awtk_indev->p_sem);
    drv_touch_disable_int();

}


static void gt9147_entry(void *parameter)
{
    static struct       rt_touch_data *read_data;
    static struct       rt_touch_info info;

    rt_awtk_port_indev_t *p_rt_awtk_indev = (rt_awtk_port_indev_t *)parameter;

    rt_device_control(p_rt_awtk_indev->p_indev, RT_TOUCH_CTRL_GET_INFO, &info);

    read_data = (struct rt_touch_data *)rt_malloc(sizeof(struct rt_touch_data) * info.point_num);

    drv_touch_listen(gt911_interrupt,p_rt_awtk_indev);

    while (1)
    {
        rt_sem_take(p_rt_awtk_indev->p_sem, RT_WAITING_FOREVER);

        if (rt_device_read(p_rt_awtk_indev->p_indev, 0, read_data, info.point_num) == info.point_num)
        {
            for (rt_uint8_t i = 0; i < info.point_num; i++)
            {
                if (read_data[i].event == RT_TOUCH_EVENT_DOWN || read_data[i].event == RT_TOUCH_EVENT_MOVE)
                {
                    rt_kprintf("%d %d %d %d %d\n", read_data[i].track_id,
                               read_data[i].x_coordinate,
                               read_data[i].y_coordinate,
                               read_data[i].timestamp,
                               read_data[i].width);
                     _g_is_pressed = RT_TRUE;
                }else{

                    _g_is_pressed = RT_FALSE;

                }

                _g_last_x = read_data[i].x_coordinate;
                _g_last_y = read_data[i].y_coordinate;

            }
        }
         drv_touch_enable_int();
        //rt_device_control(dev, RT_TOUCH_CTRL_ENABLE_INT, RT_NULL);
    }
}
int rt_awtk_port_indev_init(void)
{
    rt_thread_t  gt9147_thread = RT_NULL;
  
    rt_awtk_port_indev_t *p_rt_awtk_indev = (rt_awtk_port_indev_t *)rt_malloc(sizeof(rt_awtk_port_indev_t));
    

    void *id;
    p_rt_awtk_indev->p_indev = rt_device_find("gt9xx");

    if ( p_rt_awtk_indev->p_indev== RT_NULL){
        rt_kprintf("can't find indev device:%s\n");
        return -1;
    }

    if (rt_device_open(p_rt_awtk_indev->p_indev, RT_DEVICE_FLAG_INT_RX) != RT_EOK){
        rt_kprintf("open device failed!");
        return -1;
    }

    id = rt_malloc(sizeof(rt_uint8_t) * 8);
    rt_device_control(p_rt_awtk_indev->p_indev, RT_TOUCH_CTRL_GET_ID, id);
    rt_uint8_t * read_id = (rt_uint8_t *)id;
    rt_kprintf("id = %d %d %d %d \n", read_id[0] - '0', read_id[1] - '0', read_id[2] - '0', read_id[3] - '0');
    // rt_device_control(p_rt_awtk_indev->p_indev, RT_TOUCH_CTRL_SET_X_RANGE, (void *)&x);  /* if possible you can set your x y coordinate*/
    // rt_device_control(p_rt_awtk_indev->p_indev, RT_TOUCH_CTRL_SET_Y_RANGE, (void *)&y);
    // rt_device_control(p_rt_awtk_indev->p_indev, RT_TOUCH_CTRL_GET_INFO, id);
    // rt_kprintf("range_x = %d \n", (*(struct rt_touch_info*)id).range_x);
    // rt_kprintf("range_y = %d \n", (*(struct rt_touch_info*)id).range_y);
    // rt_kprintf("point_num = %d \n", (*(struct rt_touch_info*)id).point_num);
    rt_free(id);
    rt_device_set_rx_indicate(p_rt_awtk_indev->p_indev, rx_callback);
    p_rt_awtk_indev->p_sem = rt_sem_create("dsem", 0, RT_IPC_FLAG_FIFO);
    if (p_rt_awtk_indev->p_sem == RT_NULL){
        rt_kprintf("create dynamic semaphore failed.\n");
        return -1;
    }

    gt9147_thread = rt_thread_create("gt9xx",
                                     gt9147_entry,
                                     p_rt_awtk_indev,
                                     4096,
                                     25,
                                     5);

    if (gt9147_thread != RT_NULL)
        rt_thread_startup(gt9147_thread);

    return 0;
}

#endif